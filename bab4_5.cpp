#include <iostream>
using namespace std;

int main ()
{
        float v, t, jarak; //deklarasi
        
        cout << " Masukkan kecepatan dalam km/jam : ";
        cin >> v; //input v
        cout << " Masukkan waktu yang ditempuh dalam satuan jam : ";
        cin >> t; //input t

        jarak = v * t; //rumus

        cout << " Jarak yang ditempuh adalah " << jarak << " km/jam" << endl; //menampilkan hasil

        return 0;

}