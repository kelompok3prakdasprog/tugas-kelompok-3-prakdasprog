#include <iostream>
using namespace std;

 int pil;
 float a, l, p, L, phi=3.14, d1, d2, t, sa, sb, k, r, d, s, sc, sd;
 char back;

 void persegi(){
 	 cout << "Rumus Persegi" << endl;
    cout << "Masukan Panjang Sisi : ";
    cin >> s;
    cout << endl;
    //Rumus
    L = s*s;
    k = 4*s;
    cout << "Luas Persegi     : " << L << endl;
    cout << "Keliling Persegi : " << k << endl;
 }
 void persegipanjang(){
 	cout << "Rumus Persegi Panjang" << endl;
    cout << "Masukan Panjang Sisi : ";
    cin >> p;
    cout << "Masukan Lebar Sisi   : ";
    cin >> l;
    cout << endl;
    //Rumus
    L = p*l;
    k = 2*(p+l);
    cout << "Luas Persegi Panjang     : " << L << endl;
    cout << "Keliling Persegi Panjang : " << k << endl;
 }
 void lingkaran(){
 	 cout << "Rumus Lingkaran" << endl;
    cout << "Masukan Jari Jari : ";
    cin >> r;
    cout << "Masukan Diameter  :";
    cin >> d;
    cout << endl;
    //Rumus
    L = phi*r*r;
    k = 0.5*d;
    cout << "Luas Lingkaran     : " << L << endl;
    cout << "Keliling Lingkaran : " << k << endl;
 }
 void segitiga(){
 	 cout << "Rumus Segitiga" << endl;
    cout << "Masukan Alas   : ";
    cin >> a;
    cout << "Masukan Tinggi : ";
    cin >> t;
    cout << "Masukan Sisi   :";
    cin >> s;
    cout << endl;
    //Rumus
    L = 0.5*a*t;
    k = 3*s;
    cout << "Luas Segitiga     : " << L << endl;
    cout << "Keliling Segitiga : " << k << endl;
 }
 void jajarangenjang(){
 	 cout << "Rumus Jajarangenjang" << endl;
    cout << "Masukan Panjang Sisi a:";
    cin >> sa;
    cout << "Masukan Panjang Sisi b:";
    cin >> sb;
    cout << "Masukan Tinggi        :";
    cin >> t;
    cout << "Masukan Luas Alas     :";
    cin >> a;
    cout << endl;
    //Rumus
    L = a*t;
    k = 2*(sa+sb);
    cout << "Luas Jajarangenjang       : " << L << endl;
    cout << "Keliling Jajarangenjang   : " << k << endl;
 }
 void belahketupat(){
 	cout << "Rumus Belah Ketupat" << endl;
    cout << "Masukan Panjang Diagonal 1:";
    cin >> d1;
    cout << "Masukan Panjang Diagonal 2:";
    cin >> d2;
    cout << "Masukan Panjang Sisi      :";
    cin >> s;
    cout << endl;
    //Rumus
    L = 0.5*d1*d2;
    k = s*4;
    cout << "Luas Belah Ketupat     : " << L << endl;
    cout << "Keliling Belah Ketupat : " << k << endl;
 }
 void layanglayang(){
 	cout << "Rumus Layang Layang" << endl;
    cout << "Masukan Panjang Diagonal 1:";
    cin >> d1;
    cout << "Masukan Panjang Diagonal 2:";
    cin >> d2;
    cout << "Masukan Panjang Sisi a    :";
    cin >> sa;
    cout << "Masukan Panjang Sisi c    :";
    cin >> sc ;
    cout << endl;
     //Rumus
    sb = sa;
    sd = sc;
    L  = 0.5 * d1 *d2;
    k  = sa + sb + sc + sd;
    cout << "Luas Layang-Layang     : " << L << endl;
    cout << "Keliling Layang-Layang : " << k << endl;
 }
 void trapesium(){
 	 cout << "Rumus Trapesium" << endl;
    cout << "Masukan Panjang Sisi a:";
    cin >> sa;
    cout << "Masukan Panjang Sisi b:";
    cin >> sb;
    cout << "Masukan Tinggi        :";
    cin >> t;
    cout << "Masukan Luas Alas     :";
    cin >> a;
    cout << endl;
    // Rumus
    L = (sa + sb) * t / 2;
    k = sa * 4;
    cout << "Luas Trapesium     : " << L << endl;
    cout << "Keliling Trapesium : " << k << endl;
}
int main(){

	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	do{
  cout << "Pilih Bangun Datar : " << endl;
  cout << "1. Persegi" << endl;
  cout << "2. Persegi Panjang" << endl;
  cout << "3. Lingkaran" << endl;
  cout << "4. Segitiga" << endl;
  cout << "5. JajaranGenjang" << endl;
  cout << "6. Belah Ketupat" << endl;
  cout << "7. Layang-Layang" << endl;
  cout << "8. Trapesium" << endl;
  cout << "9. Keluar" << endl;

  cout << "Silahkan Pilih : ";
  cin >> pil;

  switch(pil){
  	case 1:
  	persegi();
  	break;
  	case 2:
  	persegipanjang();
  	break;
  	case 3:
  	lingkaran();
  	break;
  	case 4:
  	segitiga();
  	break;
  	case 5:
  	jajarangenjang();
  	break;
  	case 6:
  	belahketupat();
  	break;
  	case 7:
  	layanglayang();
  	break;
  	case 8:
  	trapesium();
  	break;

  	default:
  	cout << "Maaf PIlihan Anda Tidak Tersedia" << endl;
  }cout << "Kembali Ke Menu Utama? (y/n)";
  cin >> back;  
	}while(back == 'y' || back == 'Y');
}

