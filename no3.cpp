#include <iostream>
using namespace std;

int main(){
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 3====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	int r,d;
	float phi=3.14, keliling1, keliling2;

	cout << "Keliling Lingkaran" << endl;
	cout << "Masukan Jari-Jari Lingkaran : ";
	cin >> r;
	cout << "Masukan Diameter : ";
	cin >> d;
	keliling1 = 2*phi*r ;
	keliling2 = phi*d;
	/*
	keliling1 = Apabila yang diketahui adalah jari jari lingkaran tersebut
	keliling2 = Apabila yang diketahui adalah diameter lingkaran tersebut
	*/
	cout << "Keliling Lingkaran Tersebut Adalah " << keliling1 << endl;
	cout << "Keliling Lingkaran Tersebut Adalah " << keliling2 << endl;
}