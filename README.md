# Tugas Kelompok 3 Prakdasprog

BAB 3

1:
- Nomor Account type data Integer
- Nama Nasabah type data String
- Alamat Nasabah type data String
- Kota Nasabah type data String
- Nomor Telepon Nasabah type data Integer

2:
- Nomor penerbangan type data String
- Bandara asal type data String
- Bandara tujuan type data String
- Tanggal keberangkatan type data Integer
- Jam keberangkatan type data Integer
- Jam Datang type data Integer

3:
- a) V <- 4/3 * phi * r * r * r
- b) x <- (-b + 2*c*c + 4*a*b)/2*c
- c) m <- ((a-b)/(3*a*c)) * (1-b/c*d)

4:
- perbedaan n ← n + 2 dari sudut pandang algoritma dan n=n + 2 dari sudut pandang matematika.

Pada sudut pandang algoritma atau pemrograman, setiap ekspresi operasi matematika akan dikerjakan terlebih dahulu kemudian baru hasilnya dimasukan ke sebuah variabel container-nya. Misal pada soal persamaan n ← n + 2 atau sama saja dengan n = n + 2 atau pada program bisa juga ditulis n+=2, artinya yang dikerjakan adalah nilai n+2 dahulu kemudian hasilnya disimpan di dalam variabel n lagi.

Misalnya diketahui n = 0, kemudian setelah itu dituliskan n = n + 2, maka n + 2 akan dikerjakan terlebih dahulu sehingga 0 + 2 = 2. Nilai 2 ini kemudian dimasukan kembali ke variabel n sehingga nilai n sekarang adalah 2.

Pada sudut pandang matematika, persamaan n = n + 2 adalah sebuah persamaan yang valid jika dan hanya jika n adalah tak berhingga. Sesuatu yang tak berhingga jika ditambah berapapun hasilnya akan tetap tak berhingga.

Kesimpulan :
perbedaan n ← n + 2 dari sudut pandang algoritma dan n=n + 2 dari sudut pandang matematika adalah pada sudut pandang algoritma persamaan matematika dibaca sebagai suatu proses yang dijalankan dimulai dari ekspresi operasi matematisnya terlebih dahulu, sedangakan pada sudut pandang matematika persamaan dianggap sebagai suatu kesatuan yang variabel pada ruas kanan dan kiri harus setimbang.

5:
- DEKLARASI
- A,B : Integer

- ALGORITMA :
- A ← 10	
> A=10
- B ← 2*A-5	
> B=2(10)-5=15
- A ← B		
> A=15
- A ← A+2	
> A=15+2=17
- B ← A+B	
> B=17+15=32

Jadi nilai A dan B secara berturut-turut adalah 17 dan 32

6:
- DEKLARASI
- m : constanta Integer
- p : Integer
 
- ALGORITMA :
- m ← m + p 

instruksi ini benar karena hasil ekspresi bertipe sama dengan tipe peubah di ruas kiri 
misalnya :
- m ← 6
- p ← 4
- m ← m + p	
> m = 6 + 4 = 10

-----------------------------------------------
BAB 4

1.
#include <iostream>
using namespace std;

int main ()
{
        string nama, kota; //deklarasi

        cout << " Halo, siapa namamu? "; //menampilkan
        getline (cin, nama); //input nama
        cout << " Di kota apa kamu sekarang? "; //menampilkan
        getline (cin, kota); //input kota

        cout << " Senang berteman denganmu " << nama << ", di kota " << kota << endl; //menampilkan hasil

        return 0;;

}

2.
#include <iostream>
using namespace std;

int main ()
{
        string a;
        cout << "1. bujursangkar" << endl;
        cout << "2. segitiga" << endl;
        cout << "3. trapesium" <<endl;
        cout << "Pilih bangun yang ingin dihitung luasnya : ";
        getline(cin, a);
        if (a == "bujursangkar")
        {
                float sisi,luas1;
                cout << "Masukkan sisi : ";
                cin >> sisi;
                luas1 = sisi * sisi;
                cout << "Luasnya adalah : " << luas1 << endl;
        }
        else if (a == "segitiga")
        {
                float alas,tinggi,luas2;
                cout << "Masukkan alas : ";
                cin >> alas;
                cout << "Masukkan tinggi : ";
                cin >> tinggi;
                luas2 = alas * tinggi / 2;
                cout << "Luasnya adalah : " << luas2 << endl;
        }
        else if (a == "trapesium")
        {
                float sisia, sisib, tinggi2, luas3;
                cout << "Masukkan sisi a : ";
                cin >> sisia;
                cout << "Masukkan sisi b : ";
                cin >> sisib;
                cout << "Masukkan tinggi : ";
                cin >> tinggi2;
                luas3 = (sisia + sisib) * tinggi2 / 2;
                cout << "Luasnya adalah : " << luas3 << endl;

        }
        else
        {
                cout << "masukkan pilihan dengan benar (contoh : bujursangkar)" << endl;
        }

        
        return 0;

}
3.
#include <iostream>
using namespace std;

int main ()
{
        float c, f; //deklarasi
        
        cout << " Masukkan suhu dalam satuan celcius : "; //menampilkan
        cin >> c; //input c

        f = 9 * c / 5 + 32; //rumus

        cout << " Suhu dalam satuan Fahrenheit adalah ;  " << f << " derajat Fahrenheit " << endl; //menampilkan hasil

        return 0;

}
4.
#include <iostream>
using namespace std;

int main ()
{
        float c, f; //deklarasi
        
        cout << " Masukkan suhu dalam satuan Fahrenheit : "; //menampilkan
        cin >> f; //input f

        c = (f - 32) * 5 / 9; //rumus

        cout << " Suhu dalam satuan Celcius adalah ;  " << c << " derajat Celcius " << endl; //menampilkan hasil

        return 0;

}
5.
#include <iostream>
using namespace std;

int main ()
{
        float v, t, jarak; //deklarasi
        
        cout << " Masukkan kecepatan dalam km/jam : ";
        cin >> v; //input v
        cout << " Masukkan waktu yang ditempuh dalam satuan jam : ";
        cin >> t; //input t

        jarak = v * t; //rumus

        cout << " Jarak yang ditempuh adalah " << jarak << " km/jam" << endl; //menampilkan hasil

        return 0;

}
6.
Analisis:

input : a (alas) dan t (tinggi)

luas = a*t/2

Algoritma :

Masukan nilai alas segitiga (a) dan nilai tinggi segitiga (t),
menghitung luasnya dengan alas dan tinggi tertentu.
Luas segitiga dihitung dengan rumus L= 1/2*a*t,
nilai luas (L) di cetak sebagai output ke piranti keluaran

#include <iostream>
#include <conio.h>
using namespace std;
int main()
  {
    float luas;
    int t,a;
    
    cout<<"Masukan Alas : ";
    cin>>a;
    cout<<"Masukan Tinggi : ";
    cin>>t;
    cout<<endl;
    
    luas=0.5*a*t;
    cout<<"Luas Segitiga = "<<luas<<endl;
  getch();
  }
  7.
 1.	Masukkan kode barang
2.	Masukkan harga barang
3.	Masukkan jumlah barang
4.	Hitung bayar = harga barang*jumlah  barang
5.	Jika membayar >100 ribu maka akan diberikan diskon 10% selain itu tidak akan mendapatkan diskon
6.	Hitung total bayar = total pembayaran - diskon
7.	Cetak total bayar atau total harga akhir setelah diskon.

#include <iostream> 
using namespace std; 
int main () 
{ 
char kode_barang[20]; 
double harga_barang, 
jumlah_barang, 
diskon,
total_harga_akhir,
total_harga_awal,
jumlah_uang,
kembalian;
/*INPUT DATA*/
cout<<"\t\t\t\t==================================\n";
cout<<"\t\t\t\tPROGRAM DISKON 5%\n";
cout<<"\t\t\t\t==================================\n";
cout<<"\t\t\t\tMasukkan Kode Barang :";
cin>>kode_barang;
cout<<"\t\t\t\tMasukkan Jumlah Barang :";
cin>>jumlah_barang;
cout<<"\t\t\t\tMasukkan Harga Barang :";
cin>>harga_barang;
cout<<"\t\t\t\t==================================\n";

/*HARGA AWAL*/
total_harga_awal=jumlah_barang*harga_barang;
cout<<"\t\t\t\tTotal Harga Awal :"<<total_harga_awal<<endl;
cout<<"\t\t\t\t==================================\n";
/*HARGA DISKON*/
if (total_harga_awal>=100000)
{
diskon=total_harga_awal*5/100;
cout<<"\t\t\t\tDiskon 5% :"<<diskon<<endl;
}
/*TANPA DISKON*/
else if (total_harga_awal<=100000)
{
diskon=total_harga_awal*0/100;
cout<<"\t\t\t\tDiskon 5% :"<<diskon<<endl;
}

/*HARGA AKHIR*/
total_harga_akhir=total_harga_awal-diskon;
cout<<"\t\t\t\tTotal Harga :"<<total_harga_akhir<<endl;
cout<<"\t\t\t\tUang Yang Dibayar :";
cin>>jumlah_uang;
/*PENGEMBALIAN UANG*/
kembalian=jumlah_uang-total_harga_akhir;
cout<<"\t\t\t\tUang Kembali :"<<kembalian<<endl;
cout<<"\t\t\t\t==================================\n";
cout<<"\t\t\t\tTerima Kasih\n";
cout<<"\t\t\t\t==================================\n";
return 0;
}

8.
	#include <iostream>
	using namespace std;
	int main()
	{
	    int m,n,r;
	    cout<<"# Bunga Tunggal #\n\n";
	    cout<<"Input modal awal tabungan/pinjaman = ";
	    cin>>m;
	    cout<<"Input bunga/thn (%) = ";
	    cin>>r;
	    cout<<"Input lama tabungan/pinjaman (thn) = ";
	    cin>>n;
	    cout<<"\nTotal bunga dalam "<<n<<" tahun adalah Rp."<<n*r*m/100<<endl;
	    cout<<"Jumlah tabungan/pinjaman anda dalam "<<n<<" tahun adalah Rp."<<m+n*r*m/100<<endl;
	    return 0;
	}


-----------------------------------------------
BAB 5
1.
#include <iostream>
#include <string>
using namespace std;

int main(){
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 1====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	string nama;
	cout << "Halo, Siapa namamu?" << endl;
	cout << "Jawabanmu : ";
	getline (cin, nama);
	cout << endl;
	string tempat;
	cout << "Di kota apa kamu sekarang?" << endl;
	cout << "Jawabanmu :";
	getline (cin, tempat);
	cout << endl;

	cout << "Senang bertemu denganmu " << nama << " di kota " << tempat << endl;
}

2.
a. #include <iostream>
using namespace std;

 int pil;
 float a, l, p, L, phi=3.14, d1, d2, t, sa, sb, k, r, d, s, sc, sd;
 char back;

 void persegi(){
 	 cout << "Rumus Persegi" << endl;
    cout << "Masukan Panjang Sisi : ";
    cin >> s;
    cout << endl;
    //Rumus
    L = s*s;
    k = 4*s;
    cout << "Luas Persegi     : " << L << endl;
    cout << "Keliling Persegi : " << k << endl;
 }
 void persegipanjang(){
 	cout << "Rumus Persegi Panjang" << endl;
    cout << "Masukan Panjang Sisi : ";
    cin >> p;
    cout << "Masukan Lebar Sisi   : ";
    cin >> l;
    cout << endl;
    //Rumus
    L = p*l;
    k = 2*(p+l);
    cout << "Luas Persegi Panjang     : " << L << endl;
    cout << "Keliling Persegi Panjang : " << k << endl;
 }
 void lingkaran(){
 	 cout << "Rumus Lingkaran" << endl;
    cout << "Masukan Jari Jari : ";
    cin >> r;
    cout << "Masukan Diameter  :";
    cin >> d;
    cout << endl;
    //Rumus
    L = phi*r*r;
    k = 0.5*d;
    cout << "Luas Lingkaran     : " << L << endl;
    cout << "Keliling Lingkaran : " << k << endl;
 }
 void segitiga(){
 	 cout << "Rumus Segitiga" << endl;
    cout << "Masukan Alas   : ";
    cin >> a;
    cout << "Masukan Tinggi : ";
    cin >> t;
    cout << "Masukan Sisi   :";
    cin >> s;
    cout << endl;
    //Rumus
    L = 0.5*a*t;
    k = 3*s;
    cout << "Luas Segitiga     : " << L << endl;
    cout << "Keliling Segitiga : " << k << endl;
 }
 void jajarangenjang(){
 	 cout << "Rumus Jajarangenjang" << endl;
    cout << "Masukan Panjang Sisi a:";
    cin >> sa;
    cout << "Masukan Panjang Sisi b:";
    cin >> sb;
    cout << "Masukan Tinggi        :";
    cin >> t;
    cout << "Masukan Luas Alas     :";
    cin >> a;
    cout << endl;
    //Rumus
    L = a*t;
    k = 2*(sa+sb);
    cout << "Luas Jajarangenjang       : " << L << endl;
    cout << "Keliling Jajarangenjang   : " << k << endl;
 }
 void belahketupat(){
 	cout << "Rumus Belah Ketupat" << endl;
    cout << "Masukan Panjang Diagonal 1:";
    cin >> d1;
    cout << "Masukan Panjang Diagonal 2:";
    cin >> d2;
    cout << "Masukan Panjang Sisi      :";
    cin >> s;
    cout << endl;
    //Rumus
    L = 0.5*d1*d2;
    k = s*4;
    cout << "Luas Belah Ketupat     : " << L << endl;
    cout << "Keliling Belah Ketupat : " << k << endl;
 }
 void layanglayang(){
 	cout << "Rumus Layang Layang" << endl;
    cout << "Masukan Panjang Diagonal 1:";
    cin >> d1;
    cout << "Masukan Panjang Diagonal 2:";
    cin >> d2;
    cout << "Masukan Panjang Sisi a    :";
    cin >> sa;
    cout << "Masukan Panjang Sisi c    :";
    cin >> sc ;
    cout << endl;
     //Rumus
    sb = sa;
    sd = sc;
    L  = 0.5 * d1 *d2;
    k  = sa + sb + sc + sd;
    cout << "Luas Layang-Layang     : " << L << endl;
    cout << "Keliling Layang-Layang : " << k << endl;
 }
 void trapesium(){
 	 cout << "Rumus Trapesium" << endl;
    cout << "Masukan Panjang Sisi a:";
    cin >> sa;
    cout << "Masukan Panjang Sisi b:";
    cin >> sb;
    cout << "Masukan Tinggi        :";
    cin >> t;
    cout << "Masukan Luas Alas     :";
    cin >> a;
    cout << endl;
    // Rumus
    L = (sa + sb) * t / 2;
    k = sa * 4;
    cout << "Luas Trapesium     : " << L << endl;
    cout << "Keliling Trapesium : " << k << endl;
}
int main(){

	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	do{
  cout << "Pilih Bangun Datar : " << endl;
  cout << "1. Persegi" << endl;
  cout << "2. Persegi Panjang" << endl;
  cout << "3. Lingkaran" << endl;
  cout << "4. Segitiga" << endl;
  cout << "5. JajaranGenjang" << endl;
  cout << "6. Belah Ketupat" << endl;
  cout << "7. Layang-Layang" << endl;
  cout << "8. Trapesium" << endl;
  cout << "9. Keluar" << endl;

  cout << "Silahkan Pilih : ";
  cin >> pil;

  switch(pil){
  	case 1:
  	persegi();
  	break;
  	case 2:
  	persegipanjang();
  	break;
  	case 3:
  	lingkaran();
  	break;
  	case 4:
  	segitiga();
  	break;
  	case 5:
  	jajarangenjang();
  	break;
  	case 6:
  	belahketupat();
  	break;
  	case 7:
  	layanglayang();
  	break;
  	case 8:
  	trapesium();
  	break;

  	default:
  	cout << "Maaf PIlihan Anda Tidak Tersedia" << endl;
  }cout << "Kembali Ke Menu Utama? (y/n)";
  cin >> back;  
	}while(back == 'y' || back == 'Y');
}

b. #include <iostream>
using namespace std;

int main(){
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	float c, f;
	cout << "Masukan Suhu (Celcius) : ";
	cin >> c;

	f = (c*1.8)+32;
	cout << "Hasil konversi suhu dari Celcius ke Fahrenhait adalah " << f << endl;
}

c. #include <iostream>
using namespace std;

int main(){
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	float c, f;
	cout << "Masukan Suhu (Fahrenhait) : ";
	cin >> f;

	c = 0.5*(f-32);
	cout << "Hasil konversi suhu dari Celcius ke Fahrenhait adalah " << c << endl;
}

d. #include <iostream>
#include <string>
using namespace std;

int main(){

	int v, t, s;
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	cout << "Kecepatan mobil tersebut : ";
	cin >> v; //Km/Jam
	cout << "Waktu yang ditempuh mobil tersebut : ";
	cin >> t; //Jam

	s = v*t;

	cout << "Jarak yang ditempuh oleh mobil tersebut adalah " << s << " Km" << endl;
}

e. #include <iostream>
using namespace std;

int main(){
	float luas, alas, tinggi;
	int sisi, keliling;
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	cout << "Rumus Segitiga" << endl;
	cout << "Masukan Sisi Segitiga : ";
	cin >> sisi;
	cout << "Masukan Alas Segitiga : ";
	cin >> alas;
	cout << "Masukan Tinggi Segitiga : ";
	cin >> tinggi;

	luas = 0.5*alas*tinggi;
	keliling = sisi*3;

	cout << "Luas Segitiga Tersebut Adalah " << luas << endl;
	cout << "Ukuran Segitiga Tersebut Adalah " << keliling << endl;

}

f. #include <iostream>
using namespace std;

int main(){
	float ha, hd;
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	cout << "Harga Barang Yang Dibeli : ";
	cin >> ha;
	hd = 0.05*ha;
	ha = ha-hd;
	cout << endl;

	cout << "Diskon yang didapatkan pembeli adalah Rp." << hd << endl;
	cout << "Total yang harus dibayarkan oleh pembeli adalah Rp." << ha << endl;
}

g. #include <iostream>
using namespace std;

int main(){
	float p,x,pd,us;
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 2====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	cout << "Uang yang disimpan selama setahun: ";
	cin >> x;
	cout << "Besarnya pajak setahun: ";
	cin >> p;
	p = p/100;
	pd = x*p;
	us = x-pd;
	cout << endl;
	cout << "Pajak yang diterima nasabah adalah sebesar Rp." << pd << endl;
	cout << "Total tabungan nasabah adalah Rp." << us << endl;
	} 

3.
#include <iostream>
using namespace std;

int main(){
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 3====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	int r,d;
	float phi=3.14, keliling1, keliling2;

	cout << "Keliling Lingkaran" << endl;
	cout << "Masukan Jari-Jari Lingkaran : ";
	cin >> r;
	cout << "Masukan Diameter : ";
	cin >> d;
	keliling1 = 2*phi*r ;
	keliling2 = phi*d;
	/*
	keliling1 = Apabila yang diketahui adalah jari jari lingkaran tersebut
	keliling2 = Apabila yang diketahui adalah diameter lingkaran tersebut
	*/
	cout << "Keliling Lingkaran Tersebut Adalah " << keliling1 << endl;
	cout << "Keliling Lingkaran Tersebut Adalah " << keliling2 << endl;
}

4.
#include <iostream>
using namespace std;

int main(){
	float harga, p, us, pd;
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 3====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	cout << "Total harga makanan dan minuman yang dipesan : ";
	cin >> harga;
	cout << "Pajak Restoran : ";
	cin >> p;
	p = p/100;
	pd = harga*p;
	us = harga-pd;
	cout << endl;

	cout << "Pajak yang diterima oleh pelanggan sebesar Rp." << pd << endl;
	cout << "Total yang harus dibayarkan oleh pembeli sebesar Rp." << us << endl;

}

5.
#include <iostream>
using namespace std;

int main(){
	cout << "=====BAB 5=====" << endl;
	cout << "====Nomor 5====" << endl;
	cout << "=====Kel 3=====" << endl;
	cout << endl;

	int gp, ta, ti, total;
	/*
	gp = gaji pokok
	ta = tunjangan anak
	ti = tunjangan istri
	*/

	cout << "Gaji Pegawai Negeri Sipil" << endl;
	cout << "Besar Gaji Pokok : ";
	cin >> gp;
	cout << "Besar Tujangan Anak : ";
	cin >> ta;
	cout << "Besar Tujangan Istri : ";
	cin >> ti;
	total = gp+ta+ti;
	cout << "Total Gaji Pegawai Negeri Sipil Adalah " << total << endl;
}



-----------------------------------------------
BAB 6

1: Algoritma konversi dari Jam, Menit, Detik

1. Masukan jumlah durasi waktu
2. Maka untuk menghitung Hari, Jam, menit, Detik sudah di tentukan gunakan rumus
3. Rumus untuk menghitung Hari dan sisa Detik (hari=detik/86400;   detik=detik%86400;)
4. Rumus untuk menghitung Jam dan sisa Detik (jam=detik/3600;   detik=detik%3600;)
5. Rumus untuk menghitung Menit dan sisa Detik (menit=detik/60;   detik=detik%60;)
6. Maka konversi Hari, Jam, Menit, Detik akan dicetak sebagai output ke perangkat output

2: Algoritma konversi dari Tahun, Bulan, Hari

1. Masukan jumlah hari kerja
2. Maka untuk menghitung Tahun, Bulan, hari sudah di tentukan gunakan rumus
3. Rumus untuk menghitung Tahun dan sisa Hari (tahun=hari/365;  hari=hari%365;)
4. Rumus untuk menghitung Bulan dan sisa Hari (bulan=hari/30;  hari=hari%30;)
5. Maka konversi Tahun, Bulan, Hari akan dicetak sebagai output ke perangkat output
![image_no2](/uploads/16a32f44241259b41a6a36bcf90ef69b/image_no2.png)

_Gambar algoritma konversi dari tahun, bulan, hari_

3: Algoritma jarak hari pada kedua tanggal

1. Masukan tanggal sebelumnya
2.	Masukan bulan sebelumnya
3.	Masukan tahun sebelumnya
4.	Masukan tanggal sekarang
5.	Masukan bulan sekarang
6.	Masukan tahun sekarang
7.	Maka untuk menghitung tanggal, bulan, tahun yang sudah di tentukan gunakan rumus
8.	Rumus untuk menghitung tahun dan sisa hari (tgl.yy3=total/365;   total=total%365;)
9.	Rumus untuk menghitung bulan dan sisa hari (tgl.mm3=total/30;   tgl.dd3=total%30;)
10.	Maka jarak hari pada kedua tanggal akan dicetak sebagai output ke perangkat output
![image_no3](/uploads/8955cb2d3a3542adcfe64514a150d431/image_no3.png)

_Gambar algoritma jarak hari, pada kedua tanggal_

3: Algoritma Tripel bilangan bulat (X,Y,Z) menjadi (Y,Z,X)

1.  Masukan nilai X
2.	Masukan nilai Y
3.	Masukan nilai Z
4.	Maka untuk menukar nilai X, Y, dan Z yang sudah di tentukan gunakan rumus
5.	Rumus untuk menukar nilai X, Y, dan Z (temp = X;   X = Y;    Y = Z;   Z = temp;)
6.	Maka Tripel bilangan bulat (X,Y,Z) menjadi (Y,Z,X)

4: Algoritma untuk menghitung pecahan Rp.1000, Rp.100, Rp.50, Rp.25

1.  Masukan jumlah uang
2.	Maka untuk menghitung pecahan Rp.1000, Rp.100, Rp.50, Rp.25 sudah ditentukan gunakan rumus
3.	Rumus untuk menghitung pecahan Rp.1000 dan sisa uang (seribu=uang/1000;   uang=uang%1000;)
4.	Rumus untuk menghitung pecahan Rp.100 dan sisa uang (seratus=uang/100;   uang=uang%100;)
5.	Rumus untuk menghitung pecahan Rp.50 dan sisa uang (lp=uang/50;   uang=uang%50;)
6.	Rumus untuk menghitung pecahan Rp.25 dan sisa uang (dpl=uang/25;)
7.	Maka untuk menghitung pecahan Rp.1000, Rp.100, Rp.50, Rp.25 akan dicetak sebagai output ke perangkat output

 
4:	Algoritma konversi jarak
1.	Mulai
2.	Inputkan jarak(cm)
3.	Jarak inputan dibagi 100000 untuk mencari nilai km
4.	sisa jarak dibagi 100000 kemudian dibagi lagi dengan 100 untuk mencari nilai m
5.	output nilai konversi jarak dalam bentuk km, m dan cm
6.	selesai

 
5:	algoritma konversi panjang sebuah benda dari meter ke yard, kaki dan inchi.
1.	Mulai
2.	Inputkan panjang(meter)
3.	Nilai inputan dibagi dengan 0,9144 untuk mencari nilai yard
4.	 Nilai inputan dibagi dengan 0,3048 untuk mencari nilai kaki
5.	Nilai inputan dibagi dengan 0,0254 untuk mencari nilai inchi
6.	Output nilai konversi panjang dalam bentuk yard, kaki dan inchi
7.	Selesai
 
6:	Algoritma menghitung berat badan ideal 
1.	Mulai
2.	Inputkan tinggi badan
3.	Untuk menentukan berat badan ideal, tinggi badan dikurangi 100, lalu dikurangi lagi dengan 10% hasil pengurangan pertaman
4.	Output nilai berat badan ideal
5.	Selesai
 

7:	Algoritma penghitung jumlah penduduk suatu negara pada tahun tertentu
1.	Mulai
2.	Inputkan angka kelahiran
3.	Inputkan angka kematian
4.	Inputkan angka imigrasi
5.	Inputan angka emigrasi
6.	Jumlahkan angka kelahiran dengan angka imigrasi lalu kurangi dengan jumlah angka kematian dengan  angka emigrasi untuk   mendapatkan jumlah penduduk
7.	Output jumlah penduduk
8.	Selesai
 




